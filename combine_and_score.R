##############################################################################
# Project: CHS - Joint Replacement - Bayfront
# Date: 8/14/14
# Author: Sam Bussmann
# Description: Combine scores and get final ranking
# Notes: Used this 
##############################################################################


### Relative value from payer_type.R analysis
medicare <- .807-.807
medicaid <- .616-.807
commercial <- 3.052-.807
selfpay <- 0.00-.807



## Update pred.payer to reflect last known payers from pt data

pred.payer.upd<-pred.payer.nm[,,1]

### Calculatue expected value
expval<-(medicare*pred.payer.upd[,"Medicare"]+medicaid*pred.payer.upd[,"Medicaid"]+
           commercial*pred.payer.upd[,"Commercial"]+selfpay*pred.payer.upd[,"Self Pay"])

### Get familyID, Expected Value, Propensity Score together
tog<-data.frame(familyid=addr2$familyid,individualid=addr2$individualid,
                Zipcode=addr2[,"zipcode"],
                prediction=as.numeric(pred.prop.nm),expmargin=expval,
                score=as.numeric(pred.prop.nm*expval*100))

hist(pred.prop.nm)
hist(expval)
hist(tog$score,xlim=c(-.1,1),breaks=500)
plot(addr2$MinDist.std,tog$score,pch=".")

save(tog,file="combined_score_df.RData")
#load("combined_score_df.RData")

### Get AddressID, and dedup by that
## Need to use duplicate or unique here since we don't have address id

notdups<-(!duplicated(addr2[,c("housenumber","streetpredirectional",
                               "streetname","streetsuffix","streetpostdirectional",
                               "unittype","unitnumber","city","zipcode")]))
sum(notdups)
## Creating unique ids

int1<-paste(addr2$housenumber,addr2$streetpredirectional,addr2$streetname,addr2$streetsuffix,
            addr2$streetpostdirectional,addr2$unittype,addr2$unitnumber,addr2$city,addr2$zipcode,sep="@")

addrid<-factor(int1,labels=1:length(unique(int1)))

addr2_wid<-cbind(addr2,AddressID=addrid,facility=addr2$Hospital)
tog_wid<-cbind(tog,AddressID=addrid,facility=addr2$Hospital)

### Check some ids

addr2[addr2_wid$AddressID==20001,c("housenumber","streetpredirectional",
         "streetname","streetsuffix","streetpostdirectional",
         "unittype","unitnumber","city","zipcode")]

### Also, consider the value of all residents at an address, but with decreasing value for each additional 
### positive scored resident. Use exponential decay.

edecay<-function(x){
  if (max(x) > 0) { t<-x[order(x,decreasing=T)]
                    t[t<0]<-0
                    tsum<-0
                    for (i in 1:length(x)) tsum <- tsum + t[i]*exp(-(i-1))
                    } else 
                      tsum<-max(x)
  return(tsum)
}


addrby<-by(tog_wid$score,tog_wid$AddressID,
           
            function(x){
              if (max(x) > 0) { t<-x[order(x,decreasing=T)]
                                t[t<0]<-0
                                tsum<-0
                                for (i in 1:length(x)) tsum <- tsum + t[i]*exp(-(i-1))
              } else 
                tsum<-max(x)
              return(tsum)
            }
)

tog_2<-tog_wid[order(tog_wid$AddressID,tog_wid$score,decreasing=T),]
tog_3<-tog_2[!duplicated(tog_2$AddressID),]
tog_4<-data.frame(tog_3[order(tog_3$AddressID),],score_upd=as.numeric(addrby))

plot(tog_4$score,tog_4$score_upd,pch=".")

save(tog_4,file="final_score.RData")
#load("final_score.RData")

### Do some plotting

## Sum of score by decile
b<-10
qq<-unique(quantile(tog_4$score_upd, 
                    probs=seq.int(0,1, length.out=b+1)))
out <- cut(tog_4$score_upd, breaks=qq, include.lowest=TRUE, labels=as.character(c(10:1)))

a<-by(tog_4$score_upd,list(out),sum)
bb<-by(tog_4$score_upd,list(out),function(x) sum(!is.na(x)))

decile<-data.frame(decile=names(a),N=as.numeric(bb),sum_score=round(as.numeric(a),3),
                   ave_score=round(as.numeric(a)/as.numeric(bb),5),
                   lift=100*(round((as.numeric(a)/as.numeric(bb))/mean(tog_4$score_upd),3)-1))

library(gtools)
decile<-decile[mixedorder(decile$decile),]
decile

write.csv(decile,file="decile.csv",quote=F,row.names=F)
save(decile,file="decile.Rdata")

par(mfrow=c(1,1))
barplot(decile[,5],names.arg=as.character(1:10),col="lightblue",xlab="Decile",
        ylab="Percent Better than Average",
        main="Lift Chart - CHS Bayfront Joint Replacement",ylim=c(-100,300),yaxt="n")

# y-axis specifications
ticksy <-pretty(c(-100,0,100,200,300,400))
valuesy <- format(ticksy,big.mark=",",scientific=FALSE)
axis(2, at=ticksy, labels=paste0(sub("^\\s+", "",valuesy),"%"),cex.axis=0.8,las=1)



hist(tog_4$score_upd,xlim=c(-.05,2),breaks=1000,main="Histogram of Final Score")
plot(tog_4$prediction,tog_4$expmargin,pch=".",
     main="Plot of Propensity against Expected Profit",
     xlab="Propensity Prediction",ylab="Expected Profit")
mean((tog_4$score_upd>0))

points(tog_4$prediction[out==10],tog_4$expmargin[out==10],pch=".",col="red")
points(tog_4$prediction[out==1],tog_4$expmargin[out==1],pch=".",col="blue")
points(tog_4$prediction[out==2],tog_4$expmargin[out==2],pch=".",col="green")

####
